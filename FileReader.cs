﻿using System;
using System.IO;

namespace ivanov_lab1
{
    public class FileReader
    {
        public static int SizeMas(string filename)
        {
            try
            {
                int count = System.IO.File.ReadAllLines(filename).Length;
                return count;
            }
            catch
            {
                return 0;
            }
        }

        public static void Read(string filename, object[,] arr)
        {
            try
            {
                string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;

                using (StreamReader stream = new StreamReader(filename))
                {
                    int i = 0;
                    int count = SizeMas(filename);

                    StructWinds.Winds[] winds_s = new StructWinds.Winds[count];
                    StructWinds.Date[] date_s = new StructWinds.Date[count];


                    while (stream.Peek() >= 0)
                    {
                        string str = stream.ReadLine(); // читаем строку из файла 
                        string[] words = str.Split(' '); // разбиваем строку по словами 

                        date_s[i].day = Convert.ToInt32(words[0]);
                        date_s[i].month = Convert.ToInt32(words[1]);
                        winds_s[i].wind_direction = words[2];
                        winds_s[i].wind_speed = Convert.ToDouble(words[3]);

                        arr[i, 0] = date_s[i].day;
                        arr[i, 1] = date_s[i].month;
                        arr[i, 2] = winds_s[i].wind_direction;
                        arr[i, 3] = winds_s[i].wind_speed;

                        i++;
                    }
                }
            }
            catch
            {
                Console.WriteLine("Файл не найден");
            }
        }
    }
}
